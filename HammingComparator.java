import java.util.Comparator;

public class HammingComparator  implements Comparator<SearchNode> {

	@Override
	public int compare(SearchNode o1, SearchNode o2) {
		int priority = (o1.getMoves() + o1.getBoard().hamming())
				- (o2.getMoves() + o2.getBoard().hamming());
		if (priority < 0)
			return -1;
		else if (priority > 0)
			return 1;
		else
			return 0;
	}
}
