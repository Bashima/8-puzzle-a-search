import java.io.FileReader;
import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.Stack;
import java.util.Scanner;

public class Solver {
    private SearchNode goal, root;
    PriorityQueue<SearchNode> pQueue;
    Stack<Board> result;
    Comparator<SearchNode> comparator;
    
    public Solver(Board initial, int type) {
        result = new Stack<Board>();
        if (type == 1) {
            comparator = new HammingComparator();
        } else if(type == 2){
            comparator = new TilesComparator();
        }
        else
        {
            System.out.println("Wrong type.");
            return;
        }
        pQueue = new PriorityQueue<SearchNode>(100, comparator);
        root = new SearchNode(initial, 0, null);
        pQueue.offer(root);
        solve();
    }
    
    public SearchNode getGoal() {
        return goal;
    }
    
    public SearchNode getRoot() {
        return root;
    }
    
    public PriorityQueue<SearchNode> getpQueueHamming() {
        return pQueue;
    }
    
    public Stack<Board> getResult() {
        return result;
    }
    
    public void setGoal(SearchNode goal) {
        this.goal = goal;
    }
    
    public void setRoot(SearchNode root) {
        this.root = root;
    }
    
    public void setpQueueHamming(PriorityQueue<SearchNode> pQueueHamming) {
        this.pQueue = pQueueHamming;
    }
    
    public void setResult(Stack<Board> result) {
        this.result = result;
    }
    
    public void solve() {
        while (true) {
            SearchNode current = pQueue.poll();
//            System.out.println("state:  \n"+current.getBoard().toString());
            if (current.getBoard().isGoal()) {
                setGoal(current);
                return;
            }
            //			System.out.println("statenew:  \n"+current.getBoard().toString());
            Iterable<Board> neighbours = current.getBoard().neighbors();
            for (Board neighbour : neighbours) {
                
                // boolean positionParent = result.contains(neighbour);
                // if (positionParent == false) {
                if (current.getParent() != null) {
                    if (neighbour.equals(current.getParent().getBoard())== false) {
                        pQueue.offer(new SearchNode(neighbour, current
                                                    .getMoves() + 1, current));
                    }
                }
                else{
                    pQueue.offer(new SearchNode(neighbour, current
                                                .getMoves() + 1, current));
                }
                
            }
        }
        
    }
    public int moves() {
        return goal.getMoves();
    }
    
    public Iterable<Board> solution() {
        SearchNode s = new SearchNode();
        s = goal;
        result.add(s.getBoard());
        while (s.getParent() != null) {
            result.add(s.getParent().getBoard());
            s = s.getParent();
            // System.out.println(result.peek().toString());
        }
        return result;
    }
    
    public static void main(String[] args) throws Exception {
        
        FileReader input = null;
        input = new FileReader(args[0]);
        Scanner scanner = new Scanner(input);
        int size = scanner.nextInt();
        int[][] block = new int[size][size];
        
        for(int i= 0; i<size; i++)
        {
            for(int j = 0; j<size;j++)
            {
                block[i][j] = scanner.nextInt();
            }
        }
        
        input.close();
        int type = Integer.parseInt(args[1]);
        
        Board initial = new Board(block);
        if (initial.isSolvable()) {
            Solver solver = new Solver(initial,type);
            System.out.println("Minimum number of moves = " + solver.moves());
            Stack<Board> solution = new Stack<Board>();
            solution = (Stack<Board>) solver.solution();
            for (int i = 0; i < solver.moves() + 1; i++) {
                System.out.println(solution.pop().toString());
            }
        } else {
            System.out.println("Unsolvable puzzle");
        }
    }
}
