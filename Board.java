import java.io.FileReader;
import java.lang.Math;
import java.util.ArrayList;

public class Board {

	private int[][] board;
	private int size;
	private int[][] goalstate;

	public Board() {
	}

	public Board(int[][] blocks) {
		size = blocks[0].length;
		board = new int[size][size];
		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++) {
				board[i][j] = blocks[i][j];
			}
		}
		setGoalState();
	}

	public int[][] getBoard() {
		return board;
	}

	public int getSize() {
		return size;
	}

	public int[][] getGoalstate() {
		return goalstate;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public void setGoalstate(int[][] goalstate) {
		this.goalstate = goalstate;
	}

	public void setBoard(int[][] blocks) {
		size = blocks[0].length;
		board = new int[size][size];
		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++) {
				board[i][j] = blocks[i][j];
			}
		}
		setGoalState();
	}

	private void setGoalState() {
		int number = 1;
		goalstate = new int[size][size];
		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++) {
				goalstate[i][j] = number;
				number++;
			}
		}
		goalstate[size - 1][size - 1] = 0;
	}

	public int size() {
		return size;
	}

	public int hamming() {
		int distance = 0;

		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++) {
				if (board[i][j] != goalstate[i][j] && board[i][j] != 0) {
					distance++;
				}
			}
		}
		return distance;
	}

	public int tilesOutOfRowColumn() {
		int distance = 0;

		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++) {
				if (board[i][j] != goalstate[i][j] && board[i][j] != 0) {
					boolean sameColumn = false;
					boolean sameRow = false;
					for (int k = 0; k < size; k++) {
						if (board[i][j] == board[i][k]) {
							sameRow = true;
							break;
						}
					}
					for (int l = 0; l < size; l++) {
						if (board[i][j] == board[l][j]) {
							sameColumn = true;
							break;
						}
					}
					if (sameRow == true) {
						distance += 1;
					}
					if (sameColumn == true) {
						distance += 1;
					}
				}
			}
		}
		return distance;
	}

	public boolean isGoal() {
		boolean goal = true;
		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++) {
				if (board[i][j] != goalstate[i][j]) {
					goal = false;
					break;
				}
			}
		}
		return goal;
	}

    public int getInversionCount() {
        int inversions = 0;
        int tempSize = (int) Math.pow(Double.parseDouble(size + ""), 2.0);
        int[] temp = new int[tempSize];
        int k = 0;
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                temp[k] = board[i][j];
                k++;
            }
        }
        for (int i = 0; i < tempSize - 1; i++)
            for (int j = i + 1; j < tempSize; j++)
                if (temp[i] != 0 && temp[j] != 0 && temp[i] > temp[j]) {
                    inversions++;
                }
        if(size%2 ==0)
        {
            int holeX = -1;
            for (int i = 0; i < size; i++) {
                for (int j = 0; j < size; j++) {
                    if (board[i][j] == 0) {
                        holeX = i;
                        break;
                    }
                }
            }
            inversions = inversions + holeX;
        }
        return inversions;
        
    }
    
    public boolean isSolvable() {
        int inversionCount = getInversionCount();
        if (size % 2 == 0) {
            if (inversionCount % 2 == 0)
                return false;
            else
                return true;
        } else {
            if (inversionCount % 2 == 0)
                return true;
            else
                return false;
        }
    }
	public String toString() {
		String boardString = "";
		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++) {
				if (board[i][j] != 0)
					boardString = boardString + board[i][j];
				else
					boardString = boardString + " ";
				boardString = boardString + " ";
			}
			boardString = boardString + "\n";
		}
		return boardString;
	}

	public boolean equals(Board y) {
		boolean equal = true;
		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++) {
				if (this.board[i][j] != y.board[i][j]) {
					equal = false;
					break;
				}
			}
		}
		return equal;
	}

	public Iterable<Board> neighbors() {
		ArrayList<Board> boardList = new ArrayList<Board>();

		int holeX = -1;
		int holeY = -1;
		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++) {
				if (board[i][j] == 0) {
					holeX = i;
					holeY = j;
					break;
				}
			}
		}
		if (holeX == -1 || holeY == -1) {
			System.out.println("error finding hole position.");
			return null;
		}
		int[] newPosition = new int[2];
		for (int j = 0; j < 4; j++) {
			Board temp = new Board(this.board);
			newPosition = positions(j, holeX, holeY);
			if (newPosition != null) {
				temp.board[holeX][holeY] = temp.board[newPosition[0]][newPosition[1]];
				temp.board[newPosition[0]][newPosition[1]] = 0;
				boardList.add(temp);
			}
		}
		return boardList;
	}

	private int[] positions(int i, int holeX, int holeY) {
		int[] pos = new int[2];
		if (i == 0) {
			pos[0] = holeX - 1;
			pos[1] = holeY;
		}
		if (i == 1) {
			pos[0] = holeX;
			pos[1] = holeY - 1;
		}
		if (i == 2) {
			pos[0] = holeX + 1;
			pos[1] = holeY;
		}
		if (i == 3) {
			pos[0] = holeX;
			pos[1] = holeY + 1;
		}
		if (pos[0] >= 0 && pos[0] < size && pos[1] >= 0 && pos[1] < size) {
			return pos;
		}
		return null;
	}

	public static void main(String args[]) throws Exception {

		FileReader input = null;
		input = new FileReader(args[0]);
		int c = input.read();
		int size = c - 48;
		c = input.read();
		int block[][] = new int[size][size];
		int i = 0, j = 0;
		while ((c = input.read()) != -1 && i < size) {
			if (c > 47 & c < 58 || c == 10) {
				if (c != 10) {
					block[i][j] = c - 48;
					j++;
				} else {
					i++;
					j = 0;
				}
			}
		}
		input.close();

		Board myBoard = new Board(block);
		System.out.println(myBoard.toString());

		System.out.println("Hamming distance: " + myBoard.hamming());
		System.out.println("Tiles out of row column: "
				+ myBoard.tilesOutOfRowColumn());

		if (myBoard.isSolvable() == false) {
			System.out.println("it is not solvable");
			return;
		}

		System.out.println("Neighbours: ");
		ArrayList<Board> neighbour = (ArrayList<Board>) myBoard.neighbors();
		for (Board neighbourBoard : neighbour) {
			System.out.println(neighbourBoard.toString());
			System.out.println("Is this goal: " + neighbourBoard.isGoal()
					+ "\n");
		}

	}

}
