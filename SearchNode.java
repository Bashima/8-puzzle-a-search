public class SearchNode {

	private Board board;
	private int noOfMoves;
	private SearchNode parent;

	public SearchNode() {
	}

	public SearchNode(Board boards, int moves, SearchNode parents) {
		board = boards;
		noOfMoves = moves;
		parent = parents;
	}

	public Board getBoard() {
		return board;
	}

	public void setBoard(Board board) {
		this.board = board;
	}

	public int getMoves() {
		return noOfMoves;
	}

	public void setMoves(int moves) {
		this.noOfMoves = moves;
	}

	public SearchNode getParent() {
		return parent;
	}

	public void setParent(SearchNode parent) {
		this.parent = parent;
	}

}